﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace externalsort52c
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            button2.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Utils.GenerateRandomFile("Random", 100);
            Utils.GenerateReverseFile("Back", 100);
            textBox1.Text = Utils.PrintFile("Random");
            textBox2.Text = Utils.PrintFile("Back");

            button1.Enabled = false;
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OnePhaseNaturalSort sort = new OnePhaseNaturalSort();

            sort.SortFile("Random");
            textBox1.Text = Utils.PrintFile("Random");

            sort.SortFile("Back");
            textBox2.Text = Utils.PrintFile("Back");
        }
    }
}
